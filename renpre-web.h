#include <iostream>
#include <string>
#include <curl/curl.h> //your directory may be different
using namespace std;

string data; //will hold the url's contents

size_t writeCallback(char* buf, size_t size, size_t nmemb, void* up)
{ //callback must have this declaration
    //buf is a pointer to the data that curl has for us
    //size*nmemb is the size of the buffer

    for (int c = 0; c<size*nmemb; c++)
    {
        data.push_back(buf[c]);
    }
    return size*nmemb; //tell curl how many bytes we handled
}

string callWebsite(char* sURL)
{
	data="";
    CURL* curl; //our curl object

    curl_global_init(CURL_GLOBAL_ALL); //pretty obvious
    curl = curl_easy_init();

    curl_easy_setopt(curl, CURLOPT_URL, sURL);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);
    //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); //tell curl to output its progress

    curl_easy_perform(curl);

    curl_easy_cleanup(curl);
    curl_global_cleanup();
	return data;
}
int countOccurrences(string theString, char zeichen)
{
	int occurrences = 0;
	int actualChar=0;
	while(theString[actualChar]!='\0')
	{
		actualChar++;
		if(theString[actualChar]==zeichen)
			occurrences++;
	}
	return occurrences;
}
string* split(string toSplit,char delimiter)
{
	char *cstr = new char[toSplit.length() + 1];
	strcpy(cstr, toSplit.c_str());
	int countOfArrays=0;
	int lastDelimiter=0;
	int actualArray=0;
	int gesLength=0;
	for(int i=0;cstr[i]!='\0';i++)
	{
		if(cstr[i]==delimiter)
			countOfArrays++;
		gesLength++;
	}
	countOfArrays++;
	string *rString = new string[countOfArrays];
	for(int i=0;cstr[i]!='\0';i++)
	{
		if(cstr[i]==delimiter||cstr[i]=='\0')
		{
			string dummy="";
			for(int j=lastDelimiter;j<i;j++)
			{
				dummy+=cstr[j];
			}
			rString[actualArray]=dummy;
			actualArray++;
			lastDelimiter = i+1;
		}
	}
	string theLastWord="";
	for(int i=lastDelimiter;i<gesLength;i++)
	{
		theLastWord+=cstr[i];
	}
	rString[countOfArrays-1]=theLastWord;
	delete [] cstr;
	return rString;
}