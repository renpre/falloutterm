void quit()
{
  endwin();
}
const int upperOffset=6;
void printLikeFallout(int x, int y, std::string myString)
{
	char *cstr = new char[myString.length() + 1];
	strcpy(cstr, myString.c_str());
	for(int i=0;cstr[i]!='\0';i++)
	{
		char me[2]= "";
		me[0]=cstr[i];
		me[1]='\0';
		mvprintw(x,y+i,me);
		refresh();
		napms(10);
	}
	delete [] cstr;
}

//Malen des Hauptmenues
void draw_main_menue(int selected,int menue)
{
	erase();
	attrset(A_NORMAL | COLOR_PAIR(1));
	printLikeFallout(0,5,"Welcome to ROBCO Industries (TM) Termlink");
	printLikeFallout(3,5,"|"+myMenues[menue].Headline+"|");
	for(int i=0;i<myMenues[menue].pointsCount;i++)
	{
		printLikeFallout(upperOffset+i*2, 5, "["+myMenues[menue].points[i]+"]");
	}
	attrset(A_STANDOUT | COLOR_PAIR(1));
	std::string TheSelected=myMenues[menue].points[selected];
	TheSelected = "["+TheSelected+"]";
	char *cstr = new char[TheSelected.length() + 1];
	strcpy(cstr, TheSelected.c_str());
	mvprintw(upperOffset+selected*2, 5, cstr);
	attrset(A_NORMAL | COLOR_PAIR(1));
	refresh();
	delete [] cstr;
}
