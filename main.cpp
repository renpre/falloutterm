#include <stdlib.h> //noetig fuer atexit()
#include <stdio.h>
#include <curses.h>
#include <unistd.h>
#include <iostream>
#include <string.h>
#include <string>
#include <ctime>
#include <algorithm>
#include "ft-menues.h"
#include "renpre-web.h"
#include "renpre-crt.h"


void eSchutdown1()
{
	erase();
	attrset(A_NORMAL | COLOR_PAIR(2));
	printLikeFallout(LINES/2,COLS/2-8,"SHUTDOWN");
	refresh();
	attrset(A_NORMAL | COLOR_PAIR(1));

	
    napms(5000);
}

void eReactorStatus2()
{
	char *cstr;
	std::string website;
	erase();
	refresh();
	website = "https://creativecommons.tankerkoenig.de/json/list.php?lat=51.828847&lng=6.593939&rad=4&sort=price&type=e10&apikey=ebd4dc36-2804-805c-a98e-569192f5ecbe";
	//website = "https://creativecommons.tankerkoenig.de/json/list.php?lat=52.521&lng=13.438&rad=4&sort=price&type=e10&apikey=ebd4dc36-2804-805c-a98e-569192f5ecbe";
	cstr = new char[website.length() + 1];
	strcpy(cstr, website.c_str());
	website = callWebsite(cstr);
	//printLikeFallout(0,0,website);
	
	string (*tankstellenStrings)[13];
	string (*dataPairs)[13][2];
	int anzahlTankstellen=0;
	anzahlTankstellen=countOccurrences(website,'{');
	mvprintw(1,10,"%i",anzahlTankstellen+2);
	
	string *returnData = new string[anzahlTankstellen+3];
	returnData = split(website,'{');
	tankstellenStrings = new string[anzahlTankstellen][13];
	dataPairs = new string[anzahlTankstellen][13][2];
	
	printLikeFallout(0,0,"1");
	for(int i=0;i<anzahlTankstellen;i++)
	{
		int normMax=12;
		for(int j=0;j<normMax;j++)
		{
				tankstellenStrings[i][j] = split(returnData[i+2],',')[j];
			
		}
	}
	printLikeFallout(0,0,"2");
	for(int i=0;i<anzahlTankstellen;i++)
	{
		int normMax=12;
		for(int j=0;j<normMax;j++)
		{
			for(int k=0;k<2;k++)
			{
				if(countOccurrences(tankstellenStrings[i][j],':')>0)
				{
					dataPairs[i][j][k] = split(tankstellenStrings[i][j],':')[k];
					mvprintw(1,0,"%i",i);
					mvprintw(2,0,"%i",j);
					mvprintw(3,0,"%i",k);
				}
			}
		}
		printLikeFallout(4,0,dataPairs[i][2][1]);
	}
	printLikeFallout(0,0,"3");
	
	mvprintw(15,10, "%i", anzahlTankstellen);
	anzahlTankstellen--;
	bool exit=false;
	int selected=0;
	erase();
	printLikeFallout(10,10,"Name     : "+dataPairs[selected][1][1]);
	printLikeFallout(11,10,"Str.     : "+dataPairs[selected][3][1]+" "+dataPairs[selected][10][1]);
	printLikeFallout(12,10,"E10 Preis: "+dataPairs[selected][8][1]);
	mvprintw(15,10,"[JUMP TO: %i]",selected);
	while(exit==false)
	{
		timeout(100);
        switch(getch()){
			case 'a': case KEY_LEFT:
				selected++;
				if(selected>anzahlTankstellen)
					selected=anzahlTankstellen;
				mvprintw(15,10,"[JUMP TO: %i]",selected);
			break;
			case 'd': case KEY_RIGHT:
				selected--;
				if(selected<0)
					selected=0;
				mvprintw(15,10,"[JUMP TO: %i]",selected);
			break;
			case 10:
				erase();
				printLikeFallout(10,10,"Name     : "+dataPairs[selected][1][1]);
				printLikeFallout(11,10,"Str.     : "+dataPairs[selected][3][1]+" "+dataPairs[selected][10][1]);
				printLikeFallout(12,10,"E10 Preis: "+dataPairs[selected][8][1]);
				mvprintw(15,10,"[JUMP TO: %i]",selected);
			break;
			case 27: case '\t':
					exit = true;
			break;
		}
	}
	delete [] cstr;
}
void functionSelect(int iFunction,int icalledFrom=0)
{
	switch(iFunction)
	{
		case 1:
			eSchutdown1();
			break;
		case 2:
			eReactorStatus2();
			break;
		default:
			draw_main_menue(0,0);
			break;
	}
	draw_main_menue(0,icalledFrom);
}

const std::string currentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

    return buf;
}

int main(void)
{
	menueConfig();
	initscr();
	atexit(quit);
    cbreak();
    noecho();
    //scrollok(stdscr, TRUE);
    //nodelay(stdscr, TRUE);
	  nl();
	  keypad(stdscr, TRUE);
	start_color();
	init_pair(1, COLOR_GREEN, COLOR_BLACK); 
	init_pair(2, COLOR_RED, COLOR_BLACK); 
	curs_set(0);
	
	bool exit=false;
	int selected=0;
	int oSelected=0;
	int menue = 0;
	char *cstr;
	std::string dummy;
	int actualPTime=0;
	draw_main_menue(selected,0);
    while (exit==false) {
		timeout(100);
        switch(getch()){
			case 's': case KEY_DOWN:
				oSelected=selected;
				selected++;
				if(selected>myMenues[menue].pointsCount-1)
					selected=myMenues[menue].pointsCount-1;
				attrset(A_NORMAL | COLOR_PAIR(1));
				
				dummy=myMenues[menue].points[oSelected];
				dummy = "["+dummy+"]";
				cstr = new char[dummy.length() + 1];
				strcpy(cstr, dummy.c_str());
				mvprintw(upperOffset+oSelected*2, 5, cstr);
				attrset(A_STANDOUT | COLOR_PAIR(1));
				dummy=myMenues[menue].points[selected];
				dummy = "["+dummy+"]";
				cstr = new char[dummy.length() + 1];
				strcpy(cstr, dummy.c_str());
				mvprintw(upperOffset+selected*2, 5, cstr);
				delete [] cstr;
			break;
			case 'w': case KEY_UP:
				oSelected = selected;
				selected--;
				if(selected<0)
					selected=0;
				attrset(A_NORMAL | COLOR_PAIR(1));
				
				dummy=myMenues[menue].points[oSelected];
				dummy = "["+dummy+"]";
				cstr = new char[dummy.length() + 1];
				strcpy(cstr, dummy.c_str());
				mvprintw(upperOffset+oSelected*2, 5, cstr);
				delete [] cstr;
				attrset(A_STANDOUT | COLOR_PAIR(1));
				dummy=myMenues[menue].points[selected];
				dummy = "["+dummy+"]";
				cstr = new char[dummy.length() + 1];
				strcpy(cstr, dummy.c_str());
				mvprintw(upperOffset+selected*2, 5, cstr);
				delete [] cstr;
			break;
			case 10:
				if(myMenues[menue].pointMenueLink[selected]-1000>0)
				{
					functionSelect(myMenues[menue].pointMenueLink[selected]-1000,menue);
					selected=0;
				}
				else
				{
					menue=myMenues[menue].pointMenueLink[selected];
					oSelected= selected;
					selected=0;
					draw_main_menue(selected,menue);
					//mvprintw(20,5,"m:%i s:%i",menue,oSelected);
				}
			break;
			case 27: case '\t':
				if(menue!=0)
				{
					menue=0;
					draw_main_menue(selected,menue);
				}
				else
				{
					exit = true;
				}
			break;
		}
		attrset(A_NORMAL | COLOR_PAIR(1));
		dummy=myMenues[menue].points[selected];
				dummy = "[Time: "+currentDateTime()+"]";
				cstr = new char[dummy.length() + 1];
				strcpy(cstr, dummy.c_str());
				mvprintw(LINES-5,COLS-30, cstr);
				delete [] cstr;
    }
  return(0);
}


