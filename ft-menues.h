const int menueCount=3;
class MenueData
{
	public:
		std::string Headline;
		std::string points[10];
		int pointMenueLink[10];
		int pointsCount;
};
MenueData myMenues[menueCount];
void menueConfig()
{
	//Menue1
	myMenues[0].Headline = "MainMenue";
	myMenues[0].points[0]="REACTOR CONTROL";
	myMenues[0].pointMenueLink[0]=1;
	myMenues[0].points[1]="GASOLINE PRICES";
	myMenues[0].pointMenueLink[1]=1002;
	myMenues[0].points[2]="!!EMERGENCY SHUTDOWN!!";
	myMenues[0].pointMenueLink[2]=1001;
	myMenues[0].points[3]="DOOR CONTROL";
	myMenues[0].pointMenueLink[3]=2;
	myMenues[0].points[4]="TOGGLE LIGHT";
	myMenues[0].pointMenueLink[4]=1003;
	myMenues[0].pointsCount = 5;
	//Menue2
	myMenues[1].Headline = "ReactControl";
	myMenues[1].points[0]="START REACTOR";
	myMenues[1].pointMenueLink[0]=1;
	myMenues[1].points[1]="STOP  REACTOR";
	myMenues[1].pointMenueLink[1]=1;
	myMenues[1].pointsCount = 2;
	//Menue3
	myMenues[2].Headline = "DoorControl";
	myMenues[2].points[0]="  LOCK DOORS";
	myMenues[2].pointMenueLink[0]=2;
	myMenues[2].points[1]="UNLOCK DOORS";
	myMenues[2].pointMenueLink[1]=2;
	myMenues[2].pointsCount = 2;
}

