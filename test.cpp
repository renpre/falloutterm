#define _GLIBCXX_USE_CXX11_ABI 1
#include <iostream>
#include <string>

class person
{
  public:
    std::string name;
    int age;
};

int main()
{
  person a, b;
  a.name = "Calvin";
  b.name = "Hobbes";
  a.age = 30;
  b.age = 20;
  std::cout << a.name << ": " << a.age << std::endl;
  std::cout << b.name << ": " << b.age << std::endl;
  return 0;
}
